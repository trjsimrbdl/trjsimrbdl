/*
 * RBDL - Rigid Body Dynamics Library
 * Copyright (c) 2011-2012 Martin Felis <martin.felis@iwr.uni-heidelberg.de>
 *
 * Licensed under the zlib license. See LICENSE for more details.
 */

#include <iostream>

#include <rbdl.h>

using namespace RigidBodyDynamics;
using namespace RigidBodyDynamics::Math;

/**
 * Simulation of a double pendulum with mass 1kg located at x=1, y=-l, z=0.
 *
 * Instructions for plotting: see notes.txt
 */

int main(int argc, char* argv[]) {
	Model* model = NULL;

	unsigned int body_a_id, body_b_id;
	Body body_a, body_b;
	Joint joint_a, joint_b;

	double l = 5.67128182; // with x = 1 & q = 10°

	model = new Model();
	model->Init();

	model->gravity = Vector3d(0., -9.81, 0.);

	body_a = Body(1, Vector3d(0.0, -l, 0.0), Vector3d(0., 0., 0.));
	joint_a = Joint(JointTypeRevolute, Vector3d(0., 0., 1.));
	body_a_id = model->AddBody(0.0, Xtrans(Vector3d(0., 0., 0.)), joint_a,
			body_a);

	body_b = Body(1, Vector3d(0.0, -2.0*l, 0.0), Vector3d(0., 0., 0.));
	joint_b = Joint(JointTypeRevolute, Vector3d(0., 0., 1.));
	body_b_id = model->AppendBody( Xtrans(Vector3d(0.0, -l, 0.0)), joint_b,
			body_b);

	VectorNd Q = VectorNd::Ones(model->dof_count);
	Q[0] = -0.9 * M_PI;
	Q[1] = -1.1 * M_PI;
	std::cout<< Q<<"\n";
	VectorNd QDot = VectorNd::Zero(model->dof_count);
	VectorNd Tau = VectorNd::Zero(model->dof_count);
	VectorNd QDDot = VectorNd::Zero(model->dof_count);

	/**
	 * Calculate the joints' acceleration over a certain time.
	 */
	double deltaT = 0.001;
	for (double i = deltaT; i < 10; i += deltaT) {
		Q += deltaT * QDot;
		QDot += deltaT * QDDot;
		ForwardDynamics(*model, Q, QDot, Tau, QDDot);
		std::cout << i << " " << QDDot.transpose() << std::endl;
	}

	delete model;

	return 0;
}

