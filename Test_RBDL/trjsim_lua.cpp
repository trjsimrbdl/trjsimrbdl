/*
 * RBDL - Rigid Body Dynamics Library
 * Copyright (c) 2011-2012 Martin Felis <martin.felis@iwr.uni-heidelberg.de>
 *
 * Licensed under the zlib license. See LICENSE for more details.
 */

#include <err.h>
#include <fstream>
#include <iostream>
#include <stdio.h>

#include <rbdl.h>
#include <addons/luamodel/luamodel.h>

#ifndef BUILD_ADDON_LUAMODEL
#error "Error: RBDL addon BUILD_LUAMODELS not activated."
#endif

using namespace RigidBodyDynamics;
using namespace RigidBodyDynamics::Math;

/**
 * This function prints the gnuplot commands to visualize the results.
 * model ... Robot model
 * outfname ... string containing the output file name
 */
void plotResults(Model* model, std::string outfname) {
	if ("-" != outfname) {
		if (model->dof_count > 1) {
		      printf("for printing in gnuplot:\n"
			     "- position:\n"
			     "  plot '%s' u 0:1 w l t 'joint 0', '%s' u 0:2 w l t 'joint 1', ...\n"
			     "- velocity:\n"
			     "  plot '%s' u 0:%d w l t 'joint 0', '%s' u 0:%d w l t 'joint 1', ...\n"
			     "- acceleration:\n"
			     "  plot '%s' u 0:%d w l t 'joint 0', '%s' u 0:%d w l t 'joint 1', ...\n"
			     "- tau:\n"
			     "  plot '%s' u 0:%d w l t 'joint 0', '%s' u 0:%d w l t 'joint 1', ...\n",
			     outfname.c_str(), outfname.c_str(),
			     outfname.c_str(), 1*model->dof_count + 1, outfname.c_str(), 1*model->dof_count + 2,
			     outfname.c_str(), 2*model->dof_count + 1, outfname.c_str(), 2*model->dof_count + 2,
			     outfname.c_str(), 3*model->dof_count + 1, outfname.c_str(), 3*model->dof_count + 2 );
		} else {
		      printf("for printing in gnuplot:\n"
			     "- position:\n"
			     "  plot '%s' u 0:1 w l t 'pos'\n"
			     "- velocity:\n"
			     "  plot '%s' u 0:%zu w l t 'vel'\n"
			     "- acceleration:\n"
			     "  plot '%s' u 0:%zu w l t 'acc'\n"
			     "- tau:\n"
			     "  plot '%s' u 0:%zu w l t 'tau'\n",
			     outfname.c_str(),
			     outfname.c_str(), model->dof_count + 1,
			     outfname.c_str(), 2 * model->dof_count + 1,
			     outfname.c_str(), 3 * model->dof_count + 1);
		}
	}
}

/**
 * Calculate inverse dynamics of a given trajectory.
 */
int main(int argc, char* argv[]) {

	std::string infname("-");
	std::string outfname("-");
	std::string luafname("robot.lua");
	double timestep(1e-3);
	int verbosity = 0;

	///////////////////////////////////////
	// Parse input arguments
	///////////////////////////////////////

	for (int iopt(1); iopt < argc; ++iopt) {
		std::string const opt(argv[iopt]);
		if ("-i" == opt) {
			++iopt;
			if (iopt >= argc) {
				errx(EXIT_FAILURE,
						"-i requires an argument (use -h for some help)");
			}
			infname = argv[iopt];
		} else if ("-o" == opt) {
			++iopt;
			if (iopt >= argc) {
				errx(EXIT_FAILURE,
						"-o requires an argument (use -h for some help)");
			}
			outfname = argv[iopt];
		} else if ("-l" == opt) {
			++iopt;
			if (iopt >= argc) {
				errx(EXIT_FAILURE, "-l requires an argument (use -h for some help)");
			}
			luafname = argv[iopt];
		} else if ("-t" == opt) {
			++iopt;
		    if (iopt >= argc) {
		    	errx(EXIT_FAILURE, "-t requires an argument (use -h for some help)");
		    }
		    if (1 != sscanf(argv[iopt], "%lf", &timestep)) {
		    	err(EXIT_FAILURE, "sscanf(`%s'...)", argv[iopt]);
		    }
		    timestep *= 1e-3;
		} else if ("-v" == opt) {
			verbosity = 1;
		} else if ("-h" == opt) {
			printf(
					"Trajectory simulator\n"
							"usage [-i infile] [-o outfile] [-l luafile] [-vh]\n"
							"\n"
							"  -i  input file name   name of the trajectory definition file\n"
							"  -o  output file name  name of the predicted torque output file\n"
							"                        (use `-' for stdout, which is the default)\n"
							"  -l  Lua file name name of the robot description file\n"
							"                        (default is `robot.lua')\n"
							"  -t  milliseconds      timestep between samples in milliseconds\n"
							"                        (default is 1ms, i.e. a 1kHz control loop)\n"
							"  -v                    verbose mode (0 or 1)\n"
							"  -h                    this message\n"
							" Example: ./Trjsim_lua -i trj-1dof-a.txt -o out.txt -l pendulum.lua\n");
			exit(EXIT_SUCCESS);
		} else {
			errx(EXIT_FAILURE, "invalid option `%s' (use -h for some help)",
					argv[iopt]);
		}
	}

	if (verbosity > 0) {
		printf("Trajectory simulator\n"
				"input file: %s\n"
				"output file: %s\n"
				"robot file: %s\n", infname.c_str(), outfname.c_str(),
				luafname.c_str());
	}
	///////////////////////////////////////
	// read trajectory input file
	///////////////////////////////////////

	std::ifstream infile;
	std::istream * is;
	infile.open(infname.c_str());
	if (!infile)
		errx(EXIT_FAILURE, "failed to open `%s' for reading", infname.c_str());
	is = &infile;

	///////////////////////////////////////
	// Output initialization
	///////////////////////////////////////

	std::ostream* os;
	std::ofstream outfile;
	if ("-" == outfname)
		os = &std::cout;
	else {
		outfile.open(outfname.c_str());
		if (!outfile) {
			errx(EXIT_FAILURE, "failed to open `%s' for writing",
					outfname.c_str());
		}
		os = &outfile;
	}

	///////////////////////////////////////
	// create model from .lua file
	///////////////////////////////////////

	Model* model = NULL;
	model = new Model();
	model->Init();
	if (!Addons::LuaModelReadFromFile(luafname.c_str(), model, false)) {
		std::cerr << "Error loading model " << luafname.c_str() << std::endl;
		abort();
	}

	///////////////////////////////////////////////////////////
	// Calculate the inverse dynamics over a certain time.
	///////////////////////////////////////////////////////////
	size_t const ndof(model->dof_count);
	size_t const ndof2(2 * ndof);

	VectorNd Q, QDot, Tau, QDDot, lastQDot, lastQ;
	Q = QDot = Tau = QDDot = lastQ = lastQDot = VectorNd::Zero(model->dof_count);

	bool firstRound = true;
	for (size_t lineno(1); *is; ++lineno) {
		{
			std::vector<double> array;
			std::string line;
			getline(*is, line);
			std::istringstream ls(line);
			double value;
			while (ls >> value)
				array.push_back(value);

			if (ndof2 != array.size()) {
				if (Q != VectorNd::Zero(model->dof_count))
					break;
				errx(EXIT_FAILURE,
						"%s:%zu: error: expected 2*ndof = %zu entries but got %zu",
						infname.c_str(), lineno, ndof2, array.size());

			}

			Q = VectorNd::Map(&array[0], ndof);
			QDot = VectorNd::Map(&array[ndof], ndof);
			QDDot = (QDot - lastQDot) / timestep;

			if( firstRound == false ){
				InverseDynamics(*model, Q, QDot, QDDot, Tau);
				*os << /* position */lastQ.transpose() << "   " << /* velocity */lastQDot.transpose()
						<< "   " << /* acceleration */QDDot.transpose() << "   "
						<< /* torque */Tau.transpose() << "\n";
			}
		}
		lastQDot = QDot;
		lastQ = Q;
		firstRound = false;
	}
	plotResults(model, outfname);

	delete model;

	return 0;
}
