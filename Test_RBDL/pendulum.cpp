/*
 * RBDL - Rigid Body Dynamics Library
 * Copyright (c) 2011-2012 Martin Felis <martin.felis@iwr.uni-heidelberg.de>
 *
 * Licensed under the zlib license. See LICENSE for more details.
 */

#include <iostream>
#include <rbdl.h>

using namespace RigidBodyDynamics;
using namespace RigidBodyDynamics::Math;

/**
 * Simulation of a pendulum with mass 1kg located at x=1, y=-l, z=0.
 * For small phi angles the output should produce a sine.
 *
 * l = 2.0 / cos( phi ) = -2.1029
 * T = 2*pi*sqrt( l / g ) --> T = 2.9091
 *
 * Instructions for plotting:
 * 	- create a data.txt by running ./Pendulum > data.txt
 * 	- start gnuplot
 * 	- plot "data.txt" u 1:2 t 'QDDot'
 */

int main(int argc, char* argv[]) {
	Model* model = NULL;

	unsigned int body_a_id;
	Body body_a;
	Joint joint_a;

	model = new Model();
	model->Init();

	model->gravity = Vector3d(0., -9.81, 0.);

	body_a = Body(1, Vector3d(0.0, 2.0, 0.0), Vector3d(0., 0., 0.));
	joint_a = Joint(JointTypeRevolute, Vector3d(0., 0., 1.)
	);

	body_a_id = model->AddBody(0, Xtrans(Vector3d(0., 0., 0.)), joint_a,
			body_a);

	VectorNd Q = VectorNd::Zero(model->dof_count);
	Q[0] = -0.9 * M_PI;
	VectorNd QDot = VectorNd::Zero(model->dof_count);
	VectorNd Tau = VectorNd::Zero(model->dof_count);
	VectorNd QDDot = VectorNd::Zero(model->dof_count);

	/**
	 * Calculate the acceleration over a certain time.
	 */
	double deltaT = 0.001;
	for( double i = deltaT; i < 10; i += deltaT ){
		Q += deltaT * QDot;
		QDot += deltaT * QDDot;
		ForwardDynamics( *model, Q, QDot, Tau, QDDot );
		std::cout << i << "\t" << QDDot.transpose() << std::endl;
	}

	delete model;

	return 0;
}

