/*
 * RBDL - Rigid Body Dynamics Library
 * Copyright (c) 2011-2012 Martin Felis <martin.felis@iwr.uni-heidelberg.de>
 *
 * Licensed under the zlib license. See LICENSE for more details.
 */

#include <err.h>
#include <fstream>
#include <iostream>
#include <stdio.h>

#include <rbdl.h>

#ifndef BUILD_ADDON_LUAMODEL
	#error "Error: RBDL addon BUILD_LUAMODELS not activated."
#endif

#include <addons/luamodel/luamodel.h>

using namespace RigidBodyDynamics;
using namespace RigidBodyDynamics::Math;

/**
 * Inverse dynamics calculation for a pendulum. The trajectory is defined by a .txt file.
 */

/**
 * Method to print the calculated results.
 * model ... Robot model
 * outfname ... string containing the output file name
 */
void plotResults(Model* model, std::string outfname) {
	if ("-" != outfname) {
		if (model->dof_count > 1) {
			printf(
					"for printing in gnuplot:\n"
							"- position:\n"
							"  plot '%s' u 0:1 w l t 'joint 0', '%s' u 0:2 w l t 'joint 1', ...\n"
							"- velocity:\n"
							"  plot '%s' u 0:%zu w l t 'joint 0', '%s' u 0:%zu w l t 'joint 1', ...\n"
							"- acceleration:\n"
							"  plot '%s' u 0:%zu w l t 'joint 0', '%s' u 0:%zu w l t 'joint 1', ...\n"
							"- torque:\n"
							"  plot '%s' u 0:%zu w l t 'joint 0', '%s' u 0:%zu w l t 'joint 1', ...\n",
					outfname.c_str(), outfname.c_str(), outfname.c_str(),
					model->dof_count + 1, outfname.c_str(),
					model->dof_count + 2, outfname.c_str(),
					2 * model->dof_count + 1, outfname.c_str(),
					2 * model->dof_count + 2, outfname.c_str(),
					3 * model->dof_count + 1, outfname.c_str(),
					3 * model->dof_count + 2, outfname.c_str(),
					4 * model->dof_count + 1, outfname.c_str(),
					4 * model->dof_count + 2);
		} else {
			printf("for printing in gnuplot:\n"
					"- position:\n"
					"  plot '%s' u 0:1 w l t 'pos'\n"
					"- velocity:\n"
					"  plot '%s' u 0:%zu w l t 'vel'\n"
					"- acceleration:\n"
					"  plot '%s' u 0:%zu w l t 'acc'\n"
					"- torque:\n"
					"  plot '%s' u 0:%zu w l t 'tau'\n", outfname.c_str(),
					outfname.c_str(), model->dof_count + 1, outfname.c_str(),
					2 * model->dof_count + 1, outfname.c_str(),
					3 * model->dof_count + 1, outfname.c_str(),
					4 * model->dof_count + 1);
		}
	}
}

int main(int argc, char* argv[]) {

	// create model from .lua file
	Model* model = NULL;
	model = new Model();
	model->Init();
	if (!Addons::LuaModelReadFromFile("./pendulum.lua", model, false)) {
		std::cerr << "Error loading model ./pendulum.lua" << std::endl;
		abort();
	}

	// read trajectory input file
	std::ifstream infile;
	std::istream * is;
	std::string infname = "trj-1dof-a.txt";
	infile.open(infname.c_str());
	if (!infile)
		errx(EXIT_FAILURE, "failed to open `%s' for reading", infname.c_str());
	is = &infile;

	// Output initialization
	std::ostream* os;
	std::ofstream outfile;
	std::string outfname = "out.txt";

	outfile.open(outfname.c_str());
	if (!outfile) {
		errx(EXIT_FAILURE, "failed to open `%s' for writing", outfname.c_str());
	}
	os = &outfile;

	// Calculate the inverse dynamics over a certain time.
	size_t const ndof(model->dof_count);
	size_t const ndof2(2 * ndof);

	VectorNd Q = VectorNd::Zero(model->dof_count);
	VectorNd QDot = VectorNd::Zero(model->dof_count);
	VectorNd Tau = VectorNd::Zero(model->dof_count);
	VectorNd QDDot = VectorNd::Zero(model->dof_count);

	for (size_t lineno(1); *is; ++lineno) {
		{
			std::vector<double> array;
			std::string line;
			getline(*is, line);
			std::istringstream ls(line);
			double value;
			while (ls >> value)
				array.push_back(value);

			if (ndof2 != array.size()) {
				if (Q != VectorNd::Zero(model->dof_count))
					break;
				errx(EXIT_FAILURE,
						"%s:%zu: error: expected 2*ndof = %zu entries but got %zu",
						infname.c_str(), lineno, ndof2, array.size());

			}
			Q[0] = array[0];
			QDot[0] = array[ndof];
			std::cout << "Q:" << Q << "\tQDot:" << QDot << "\n";
		}
		InverseDynamics(*model, Q, QDot, QDDot, Tau);

		*os << /* position */Q << "   " << /* velocity */QDot.transpose()
				<< "   " << /* acceleration */QDDot.transpose() << "   "
				<< /* torque */Tau.transpose() << "\n";
	}

	plotResults(model, outfname);

	delete model;

	return 0;
}

