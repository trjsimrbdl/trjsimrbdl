/*
 * RBDL - Rigid Body Dynamics Library
 * Copyright (c) 2011-2012 Martin Felis <martin.felis@iwr.uni-heidelberg.de>
 *
 * Licensed under the zlib license. See LICENSE for more details.
 */

#include <err.h>
#include <fstream>
#include <iostream>
#include <stdio.h>

#include <rbdl.h>

using namespace RigidBodyDynamics;
using namespace RigidBodyDynamics::Math;

/**
 * Simulation of a pendulum with mass 1kg located at x=1, y=-l, z=0.
 * For small phi angles the output should produce a sine.
 *
 * Since T = 2*pi*sqrt( l / g ) for l = 5.67128182 --> T = 4.77, which is consistent
 * with the gnuplot.
 *
 * Instructions for plotting:
 * 	- create a data.txt by running ./Pendulum > data.txt
 * 	- start gnuplot
 * 	- plot "data.txt" u 1:2 t 'QDDot'
 */

/**
 * Method to print the calculated results.
 * model ... Robot model
 * outfname ... string containing the output file name
 */
void plotResults( Model* model, std::string outfname )
{
  if ( "-" != outfname ) {
    if (model->dof_count > 1) {
      printf("for printing in gnuplot:\n"
	     "- position:\n"
	     "  plot '%s' u 0:1 w l t 'joint 0', '%s' u 0:2 w l t 'joint 1', ...\n"
	     "- velocity:\n"
	     "  plot '%s' u 0:%zu w l t 'joint 0', '%s' u 0:%zu w l t 'joint 1', ...\n"
	     "- acceleration:\n"
	     "  plot '%s' u 0:%zu w l t 'joint 0', '%s' u 0:%zu w l t 'joint 1', ...\n"
	     "- torque without gravity compensation:\n"
	     "  plot '%s' u 0:%zu w l t 'joint 0', '%s' u 0:%zu w l t 'joint 1', ...\n"
	     "- torque with gravity compensation:\n"
	     "  plot '%s' u 0:%zu w l t 'joint 0', '%s' u 0:%zu w l t 'joint 1', ...\n",
	     outfname.c_str(), outfname.c_str(),
	     outfname.c_str(), model->dof_count + 1, outfname.c_str(), model->dof_count + 2,
	     outfname.c_str(), 2 * model->dof_count + 1, outfname.c_str(), 2 * model->dof_count + 2,
	     outfname.c_str(), 3 * model->dof_count + 1, outfname.c_str(), 3 * model->dof_count + 2,
	     outfname.c_str(), 4 * model->dof_count + 1, outfname.c_str(), 4 * model->dof_count + 2);
    }
    else {
      printf("for printing in gnuplot:\n"
	     "- position:\n"
	     "  plot '%s' u 0:1 w l t 'pos'\n"
	     "- velocity:\n"
	     "  plot '%s' u 0:%zu w l t 'vel'\n"
	     "- acceleration:\n"
	     "  plot '%s' u 0:%zu w l t 'acc'\n"
	     "- torque without gravity compensation:\n"
	     "  plot '%s' u 0:%zu w l t 'tau'\n"
	     "- torque with gravity compensation:\n"
	     "  plot '%s' u 0:%zu w l t 't+g'\n",
	     outfname.c_str(),
	     outfname.c_str(), model->dof_count + 1,
	     outfname.c_str(), 2 * model->dof_count + 1,
	     outfname.c_str(), 3 * model->dof_count + 1,
	     outfname.c_str(), 4 * model->dof_count + 1);
    }
  }
}

int main(int argc, char* argv[]) {
	Model* model = NULL;
	std::ostream* os;
	std::ofstream outfile;
	std::string outfname = "out.txt";

	unsigned int body_a_id;
	Body body_a;
	Joint joint_a;

	double l = 5.67128182; // with x = 1 & q = 10°

	model = new Model();
	model->Init();

	model->gravity = Vector3d(0., -9.81, 0.);

	body_a = Body(1, Vector3d(1.0, -l, 0.0), Vector3d(0.01, 0.01, 0.01));
	joint_a = Joint(JointTypeRevolute, Vector3d(0., 0., 1.)
	);

	body_a_id = model->AddBody(0, Xtrans(Vector3d(0., 0., 0.)), joint_a,
			body_a);

	Body body_ = model->mBodies.at(1);
	std::cout<<"*** Pendulum_trjsim_forward *** \n";
	std::cout<<"mInertia:\n"<<body_.mInertia<<"\n\n";
	std::cout<<"mSpatialInertia\n"<<body_.mSpatialInertia<<"\n";


	VectorNd Q = VectorNd::Zero(model->dof_count);
	VectorNd QDot = VectorNd::Zero(model->dof_count);
	VectorNd Tau = VectorNd::Zero(model->dof_count);
	VectorNd QDDot = VectorNd::Zero(model->dof_count);

	// Output initialization
    outfile.open(outfname.c_str());
    if ( ! outfile) {
      errx(EXIT_FAILURE, "failed to open `%s' for writing", outfname.c_str());
    }
    os = &outfile;

	/**
	 * Calculate the acceleration over a certain time.
	 */
	double deltaT = 0.001;
	for( double i = deltaT; i < 10; i += deltaT ){
		Q += deltaT * QDot;
		QDot += deltaT * QDDot;
		ForwardDynamics( *model, Q, QDot, Tau, QDDot );

	    *os << /* position */ Q << "   "
	    << /* velocity */ QDot.transpose() << "   "
		<< /* acceleration */ QDDot.transpose() << "   "
		<< /* torque */ Tau.transpose() << "\n"; // set inertia to a very small value
	}

	plotResults( model, outfname );

	delete model;

	return 0;
}

