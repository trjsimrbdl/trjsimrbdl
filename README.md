# trjsimRBDL - Trajectory Simulation Rigid Body Dynamics Library Copyright (c) 2012 Marlene Mohr, Anton Jerey, Thomas Holleis trjsimrbdl@gmail.com

# Introduction
The following project shows how the Rigid Body Dynamic Library (RBDL) was prepared and tested for the implementation into the whole-body control framework. The aim is to replace the TAO library with the RBDL, but this is a task for a future project. To make sure that the dynamics calculation of both algorithms result in the same data, both libraries were tested and compared.

# Documentation
The documentation of this project is in the repository.

# Licensing
The library is published under the very permissive zlib free software license which should allow you to use the software wherever you need.
This is the full license text (zlib license):
trjsimRBDL - Trajectory Simulaiton Rigid Body Dynamics Library
Copyright (c) 2012 Marlene Mohr, Anton Jerey, Thomas Holleis <trjsimrbdl@gmail.com>

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not
         be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
