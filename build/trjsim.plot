set xlabel 'position / m'
set ylabel 'position / m'
set grid
set terminal jpeg
set output "Comparisons/FNAME-pos.jpeg"
plot 'RBDL_plots/FNAME.data' u 0:1 w lp pi 3 t 'pos-rbdl', 'WBC_plots/wbc-FNAME.data' u 0:1 w l t 'pos-wbc' 

set xlabel 'position / m'
set ylabel 'velocity / m/s'
set grid
set terminal jpeg
set output "Comparisons/FNAME-vel.jpeg"
plot 'RBDL_plots/FNAME.data' u 0:2 w lp pi 3 t 'vel-rbdl', 'WBC_plots/wbc-FNAME.data' u 0:2 w l t 'vel-wbc'

set xlabel 'position / m'
set ylabel 'acceleration / m/ms²'
set grid
set terminal jpeg
set output "Comparisons/FNAME-acc.jpeg"
plot 'RBDL_plots/FNAME.data' u 0:3 w lp pi 3 t 'acc-rbdl', 'WBC_plots/wbc-FNAME.data' u 0:3 w l t 'acc-wbc'

set xlabel 'position / m'
set ylabel 'Nm'
set grid
set terminal jpeg
set output "Comparisons/FNAME-tau.jpeg"
plot 'RBDL_plots/FNAME.data' u 0:4 w lp pi 3 t 'tau-rbdl', 'WBC_plots/wbc-FNAME.data' u 0:5 w l t 'tau-wbc'
