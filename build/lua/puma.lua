--[[
--  This is an example model for the RBDL addon luamodel. You need to
--  enable BUILD_ADDON_LUAMODEL to be able to use this file.
--]]
print ("Parsing robot model from Lua file")
print ("...")

model = {
  gravity = {0., 0, -9.810},
  frames = {
    {
      name = "base",
      parent = "ROOT",
      body = {
	mass = 34.4,
	com = { 0.0, 0.0, 0.0 },
	inertia = { 
	  	{0.0, 0.0, 0.0},
	  	{0.0, 0.0, 0.0},
	  	{0.0, 0.0, 1.49}
	} 
      },
      joint = {
	{ 0., 0., 1., 0., 0., 0. }
      },
      joint_frame = {
	r = { 0., 0., 0. },
	E = {
          { 1.,  0.,  0.},
          { 0.,  1.,  0.},
          { 0.,  0.,  1.}
	},
      },
    },
    {
      name = "upper-arm",
      parent = "base",
      body = {
	mass = 17.4,
	com = { 0.068, 0.006, -0.016},
	inertia = { 
	  	{0.130, 0.000, 0.000},
	  	{0.000, 0.524, 0.000},
	  	{0.000, 0.000, 5.249}
	} 
      },
      joint = {
	{ 0., 0., 1., 0., 0., 0. }
      },
      joint_frame = {
	r = { 0., 0.2435, 0. },
	E = {
          {1.,  0.,  0.},
          {0.,  0.,  1.},
          {0., -1.,  0.}
	},
      },
    },
    {
      name = "lower-arm",
      parent = "upper-arm",
      body = {
	mass = 6.04,
	com = { 0, -0.143, 0.014 },
	inertia = { 
  	{0.192, 0.0000, 0.000},
  	{0.000, 0.0154, 0.000},
  	{0.000, 0.0000, 1.042 }
	} 
      },
      joint = {
	{ 0., 0., 1., 0., 0., 0. }
      },
      joint_frame = {
	r = {0.4318, 0, -0.0934 },
	E = {
          {1., 0.,  0.},
          {0., 1.,  0.},
          {0., 0.,  1.}
	},
      },
    },
{
      name = "wrist-hand",
      parent = "lower-arm",
      body = {
	mass =  0.82,
	com = { 0.0, 0.0, -0.019},
	inertia = { 
	{0.0018, 0.0000, 0.0000},
  	{0.0000, 0.0018, 0.0000},
  	{0.0000, 0.0000, 0.2013}
	} 
      },
      joint = {
	{ 0., 0., 1., 0., 0., 0. }
      },
      joint_frame = {
	r = {-0.0203, -0.4331, 0.0 },
	E = {
          {1., 0.,  0.},
          {0., 0., -1.},
          {0., 1.,  0.}
	},
      },
    },
{
      name = "wrist-finger",
      parent = "wrist-hand",
      body = {
	mass = 0.34,
	com = { 0.0, 0.0, 0.0 },
	inertia = { 
  	{0.0003, 0.0000, 0.0000},
  	{0.0000, 0.0003, 0.0000},
  	{0.0000, 0.0000, 0.1794}
	} 
      },
      joint = {
	{ 0., 0., 1., 0., 0., 0. }
      },
      joint_frame = {
	r = { 0., 0., 0. },
	E = {
          {1.,  0.,  0.},
          {0.,  0.,  1.},
          {0., -1.,  0.}
	},
      },
    },
{
      name = "end-effector",
      parent = "wrist-finger",
      body = {
	mass = 0.09,
	com = { 0.0, 0.0, 0.032},
	inertia = { 
  	{0.00015, 0.0     , 0.0    },
  	{0.0    , 0.00015 , 0.0    },
  	{0.0    , 0.0     , 0.19304}
	} 
      },
      joint = {
	{ 0., 0., 1., 0., 0., 0. }
      },
      joint_frame = {
	r = { 0, 0, 0 },
	E = {
          {1., 0.,  0.},
          {0., 0., -1.},
          {0., 1.,  0.}
	},
      },
    },
  }
}
print ("Parsing done!")
return model

