--[[
--  This is an example model for the RBDL addon luamodel. You need to
--  enable BUILD_ADDON_LUAMODEL to be able to use this file.
--]]
print ("Parsing robot model from Lua file")
print ("...")

model = {
  gravity = {0., -9.81, 0.},
  frames = {
    {
      name = "pendulum",
      parent = "ROOT",
      body = {
	mass = 1.0,
	com = { 1.0, 0.0, 0.0 },
	inertia = { 
  	{1.1, 0.1, 0.2},
  	{0.3, 1.2, 0.4},
  	{0.5, 0.6, 1.3}
	} 
      },
      joint = {
	{ 0., 0., 1., 0., 0., 0. }
      },
      joint_frame = {
	r = { 0., 0., 0. },
	E = {
          {1., 0., 0.},
          {0., 1., 0.},
          {0., 0., 1.}
	},
      },
    },
   {
      name = "pendulum2",
      parent = "pendulum",
      body = {
	mass = 1.0,
	com = { 2.0, 0.0, 0.0 },
	inertia = { 
  	{1.1, 0.1, 0.2},
  	{0.3, 1.2, 0.4},
  	{0.5, 0.6, 1.3}
	} 
      },
      joint = {
	{ 0., 0., 1., 0., 0., 0. }
      },
      joint_frame = {
	r = { 0., 0., 0. },
	E = {
          {1., 0., 0.},
          {0., 1., 0.},
          {0., 0., 1.}
	},
      },
    },
  }
}
print ("Parsing done!")
return model
