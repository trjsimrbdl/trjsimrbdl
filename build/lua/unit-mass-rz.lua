--[[
--  This is an example model for the RBDL addon luamodel. You need to
--  enable BUILD_ADDON_LUAMODEL to be able to use this file.
--]]
print ("Parsing robot model from Lua file")
print ("...")

model = {
  gravity = {0., -9.81, 0.},
  frames = {
    {
      name = "unit-mass-rz",
      parent = "ROOT",
      body = {
	mass = 1.0,
	com = { 1.0, 0.0, 0.0 },
	inertia = {
	  	{0.001, 0., 0.},
	  	{0., 0.001, 0.},
	  	{0., 0., 0.001}
	} 
      },
      joint = {
	{ 0., 1., 0., 0., 0., 0. }
      },
      joint_frame = {
	r = { 0., 0., 0. },
	E = {
          {1., 0., 0.},
          {0., 1., 0.},
          {0., 0., 1.}
	},
      },
    },
  }
}
print ("Parsing done!")
return model
