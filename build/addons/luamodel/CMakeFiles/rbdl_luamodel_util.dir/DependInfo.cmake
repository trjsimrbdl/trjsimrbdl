# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/thomas/Dropbox/Bac1project/Projectfolder/addons/luamodel/rbdl_luamodel_util.cc" "/home/thomas/Dropbox/Bac1project/Projectfolder/build/addons/luamodel/CMakeFiles/rbdl_luamodel_util.dir/rbdl_luamodel_util.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/thomas/Dropbox/Bac1project/Projectfolder/build/addons/luamodel/CMakeFiles/rbdl_luamodel.dir/DependInfo.cmake"
  "/home/thomas/Dropbox/Bac1project/Projectfolder/build/CMakeFiles/rbdl.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "src"
  "../src"
  "addons/luamodel/include/rbdl"
  "../addons/luamodel/luatables"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
