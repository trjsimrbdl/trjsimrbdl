set xlabel 'time / ms'
set ylabel 'position / m'
set grid
set terminal jpeg
set output "Delta_plotss/FNAME-pos.jpeg"
plot 'Delta_plots/FNAME.data' u 0:1 w l t 'pos-delta-joint1', 'Delta_plots/FNAME.data' u 0:2 w l t 'pos-delta-joint2'

set xlabel 'time / ms'
set ylabel 'position / m/ms'
set grid
set terminal jpeg
set output "Delta_plotss/FNAME-vel.jpeg"
plot 'Delta_plots/FNAME.data' u 0:3 w l t 'vel-delta-joint1', 'Delta_plots/FNAME.data' u 0:4 w l t 'vel-delta-joint2'

set xlabel 'time / ms'
set ylabel 'position / m/ms²'
set grid
set terminal jpeg
set output "Delta_plotss/FNAME-acc.jpeg"
plot 'Delta_plots/FNAME.data' u 0:5 w l t 'acc-delta-joint1', 'Delta_plots/FNAME.data' u 0:6 w l t 'acc-delta-joint2'

set xlabel 'time / ms'
set ylabel 'Nm'
set grid
set terminal jpeg
set output "Delta_plotss/FNAME-tau.jpeg"
plot 'Delta_plots/FNAME.data' u 0:7 w l t 'tau-delta-joint1', 'Delta_plots/FNAME.data' u 0:8 w l t 'tau-delta-joint2'
