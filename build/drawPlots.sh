# create data
./Trjsim_lua -l lua/unit-mass-rx.lua -o RBDL_plots/unit-mass-rx.data -i trj-1dof-a.txt
./Trjsim_lua -l lua/unit-mass-rz.lua -o RBDL_plots/unit-mass-rz.data -i trj-1dof-a.txt
./Trjsim_lua -l lua/unit-mass-px.lua -o RBDL_plots/unit-mass-px.data -i trj-1dof-a.txt
./Trjsim_lua -l lua/unit-mass-pz.lua -o RBDL_plots/unit-mass-pz.data -i trj-1dof-a.txt
./Trjsim_lua -l lua/unit-mass-rx-rx.lua -o RBDL_plots/unit-mass-rx-rx.data -i trj-2dof-a.txt
./Trjsim_lua -l lua/unit-mass-px-px.lua -o RBDL_plots/unit-mass-px-px.data -i trj-2dof-a.txt

# calculate deltas
./Delta_plots/calcDelta RBDL_plots/unit-mass-rx_delta.data WBC_plots/wbc-unit-mass-rx.data
./Delta_plots/calcDelta RBDL_plots/unit-mass-rz_delta.data WBC_plots/wbc-unit-mass-rz.data
./Delta_plots/calcDelta RBDL_plots/unit-mass-px_delta.data WBC_plots/wbc-unit-mass-px.data
./Delta_plots/calcDelta RBDL_plots/unit-mass-pz_delta.data WBC_plots/wbc-unit-mass-pz.data
./Delta_plots/calcDelta RBDL_plots/unit-mass-rx-rx_delta.data WBC_plots/wbc-unit-mass-rx-rx.data

# draw RBDL & WBC trjsim plots
sed 's/FNAME/unit-mass-px/g' trjsim.plot | gnuplot --persist
sed 's/FNAME/unit-mass-pz/g' trjsim.plot | gnuplot --persist
sed 's/FNAME/unit-mass-rx/g' trjsim.plot | gnuplot --persist
sed 's/FNAME/unit-mass-rz/g' trjsim.plot | gnuplot --persist
sed 's/FNAME/unit-mass-rx-rx/g' trjsim_2dof.plot | gnuplot --persist
sed 's/FNAME/unit-mass-px-px/g' trjsim_2dof.plot | gnuplot --persist
