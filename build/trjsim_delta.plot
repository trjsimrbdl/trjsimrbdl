set xlabel 'time / ms'
set ylabel 'position / m'
set grid
set terminal jpeg
set output "Delta_plots/FNAME-pos.jpeg"
plot 'Delta_plots/FNAME.data' u 0:1 w l t 'pos-delta'

set xlabel 'time / ms'
set ylabel 'position / m/ms'
set grid
set terminal jpeg
set output "Delta_plots/FNAME-vel.jpeg"
plot 'Delta_plots/FNAME.data' u 0:2 w l t 'vel-delta'

set xlabel 'time / ms'
set ylabel 'position / m/ms²'
set grid
set terminal jpeg
set output "Delta_plots/FNAME-acc.jpeg"
plot 'Delta_plots/FNAME.data' u 0:3 w l t 'acc-delta'

set xlabel 'time / ms'
set ylabel 'Nm'
set grid
set terminal jpeg
set output "Delta_plots/FNAME-tau.jpeg"
plot 'Delta_plots/FNAME.data' u 0:4 w l t 'tau-delta'
