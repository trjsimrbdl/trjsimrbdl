# draw WBC plots
sed 's/FNAME/wbc-unit-mass-px/g' wbc.plot | gnuplot --persist
sed 's/FNAME/wbc-unit-mass-pz/g' wbc.plot | gnuplot --persist
sed 's/FNAME/wbc-unit-mass-rx/g' wbc.plot | gnuplot --persist
sed 's/FNAME/wbc-unit-mass-rz/g' wbc.plot | gnuplot --persist
