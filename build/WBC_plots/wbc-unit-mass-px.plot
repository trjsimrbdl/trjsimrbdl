set xlabel 'time / ms'
set ylabel 'position / m'
set grid
set terminal jpeg
set output "wbc-unit-mass-px-pos.jpeg"
plot 'wbc-unit-mass-px.data' u 0:1 w l t 'pos'

set xlabel 'time / ms'
set ylabel 'position / m/ms'
set grid
set terminal jpeg
set output "wbc-unit-mass-px-vel.jpeg"
plot 'wbc-unit-mass-px.data' u 0:2 w l t 'vel'

set xlabel 'time / ms'
set ylabel 'position / m/ms²'
set grid
set terminal jpeg
set output "wbc-unit-mass-px-acc.jpeg"
plot 'wbc-unit-mass-px.data' u 0:3 w l t 'acc'

set xlabel 'time / ms'
set ylabel 'Nm'
set grid
set terminal jpeg
set output "wbc-unit-mass-px-tau.jpeg"
plot 'wbc-unit-mass-px.data' u 0:5 w l t 'tau'
