set xlabel 'position / m'
set ylabel 'position / m'
set grid
set terminal jpeg
set output "FNAME-pos.jpeg"
plot 'FNAME.data' u 0:1 w l t 'pos'

set xlabel 'position / m'
set ylabel 'velocity / m/s'
set grid
set terminal jpeg
set output "FNAME-vel.jpeg"
plot 'FNAME.data' u 0:2 w l t 'vel'

set xlabel 'position / m'
set ylabel 'acceleration / m/s²'
set grid
set terminal jpeg
set output "FNAME-acc.jpeg"
plot 'FNAME.data' u 0:3 w l t 'acc'

set xlabel 'position / m'
set ylabel 'Nm'
set grid
set terminal jpeg
set output "FNAME-tau.jpeg"
plot 'FNAME.data' u 0:4 w l t 'tau'
