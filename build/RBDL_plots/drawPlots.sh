# draw RBDL plots
sed 's/FNAME/unit-mass-px/g' rbdl.plot | gnuplot --persist
sed 's/FNAME/unit-mass-pz/g' rbdl.plot | gnuplot --persist
sed 's/FNAME/unit-mass-rx/g' rbdl.plot | gnuplot --persist
sed 's/FNAME/unit-mass-rz/g' rbdl.plot | gnuplot --persist
