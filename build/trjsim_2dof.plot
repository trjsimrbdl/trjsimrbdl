set xlabel 'position / m'
set ylabel 'position / m'
set grid
set terminal jpeg
set output "Comparisons/FNAME-pos.jpeg"
plot 'RBDL_plots/FNAME.data' u 0:1 w lp pi 25 t 'pos-rbdl-joint1', 'RBDL_plots/FNAME.data' u 0:2 w lp pi 25 t 'pos-rbdl-joint2', 'WBC_plots/wbc-FNAME.data' u 0:1 w l t 'pos-wbc-joint1', 'WBC_plots/wbc-FNAME.data' u 0:2 w l t 'pos-wbc-joint2' 

set xlabel 'position / m'
set ylabel 'velocity / m/s'
set grid
set terminal jpeg
set output "Comparisons/FNAME-vel.jpeg"
plot 'RBDL_plots/FNAME.data' u 0:3 w lp pi 25 t 'vel-rbdl-joint1', 'RBDL_plots/FNAME.data' u 0:4 w lp pi 25 t 'vel-rbdl-joint2', 'WBC_plots/wbc-FNAME.data' u 0:3 w l t 'vel-wbc-joint1', 'WBC_plots/wbc-FNAME.data' u 0:4 w l t 'vel-wbc-joint2'

set xlabel 'position / m'
set ylabel 'acceleration / m/ms²'
set grid
set terminal jpeg
set output "Comparisons/FNAME-acc.jpeg"
plot 'RBDL_plots/FNAME.data' u 0:5 w lp pi 25 t 'acc-rbdl-joint1', 'RBDL_plots/FNAME.data' u 0:6 w lp pi 25 t 'acc-rbdl-joint2', 'WBC_plots/wbc-FNAME.data' u 0:5 w l t 'acc-wbc-joint1', 'WBC_plots/wbc-FNAME.data' u 0:6 w l t 'acc-wbc-joint2'

set xlabel 'position / m'
set ylabel 'Nm'
set grid
set terminal jpeg
set output "Comparisons/FNAME-tau.jpeg"
plot 'RBDL_plots/FNAME.data' u 0:7 w lp pi 25 t 'tau-rbdl-joint1', 'RBDL_plots/FNAME.data' u 0:8 w lp pi 25 t 'tau-rbdl-joint2', 'WBC_plots/wbc-FNAME.data' u 0:9 w l t 'tau-wbc-joint1', 'WBC_plots/wbc-FNAME.data' u 0:10 w l t 'tau-wbc-joint2'
